import { writable } from 'svelte/store'

export const nMax = 15
export const n = writable(3)

const matrixDefault = []
for (let i = 0; i < nMax; ++i) {
    matrixDefault.push(Array(nMax).fill(false))
}
export const matrix = writable(matrixDefault)