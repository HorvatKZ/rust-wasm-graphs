run: build
	@cd client && npm run dev

build:
	@cd rust-graphs && wasm-pack build --target web
	@rm -rf client/pkg
	@cp -r rust-graphs/pkg client/pkg

test:
	@cd rust-graphs && cargo test
	@cd rust-graphs && wasm-pack test --node

deploy:
	cd rust-graphs && wasm-pack build --target web
	cp -r rust-graphs/pkg client/pkg
	cd client && npm install && npm run build
	