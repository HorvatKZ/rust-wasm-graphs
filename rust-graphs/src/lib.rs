mod utils;
extern crate queues;

use queues::*;
use std::fmt;
use wasm_bindgen::prelude::*;


#[wasm_bindgen]
pub struct Graph {
    edges: Vec<Vec<u32>>,
    n: usize
}

#[wasm_bindgen]
impl Graph {
    pub fn from_edges(n: usize, raw_edges: Vec<u32>) -> Self {
        utils::set_panic_hook();

        if raw_edges.len() % 2 != 0 {
            panic!("raw_edges should contain pairs of numbers as edges")
        }

        if n < *raw_edges.iter().max().unwrap() as usize + 1 {
            panic!("raw_edges has higher index, than n - 1")
        }

        let mut edges: Vec<Vec<u32>> = vec![Vec::new(); n];
        for (from_ind, to_ind) in (0..raw_edges.len() / 2).map(|x| (2 * x, 2 * x + 1)) {
            let (from, to) = (raw_edges[from_ind], raw_edges[to_ind]);
            edges[from as usize].push(to);
        }
        
        Graph { edges, n }
    }

    pub fn from_matrix(n: usize, matrix: Vec<u32>) -> Self {
        utils::set_panic_hook();

        if matrix.len() != n * n {
            panic!("The number of values in the matrix is not n^2")
        }

        let mut edges: Vec<Vec<u32>> = vec![Vec::new(); n];
        for i in 0..n {
            for j in 0..n {
                let is_linked = matrix[i * n + j] != 0;
                if is_linked {
                    edges[i].push(j as u32)
                }
            }
        }

        Graph { edges, n }
    }

    pub fn display(&self) -> String {
        self.to_string()
    }

    pub fn get_components(&self) -> Vec<u32> {
        let mut result = vec![0; self.n];
        let mut visited = vec![false; self.n];
        let mut q: Queue<u32> = queue![];
        let mut ind = 0;

        for i in 0..self.n {
            if visited[i] {
                continue;
            }

            let _ = q.add(i as u32);
            while q.size() != 0 {
                if let Ok(from) = q.remove() {
                    result[from as usize] = ind;
                    for j in 0..self.edges[from as usize].len() {
                        let to = self.edges[from as usize][j];
                        if !visited[to as usize] {
                            _ = q.add(to);
                        }
                    }
                    visited[from as usize] = true;
                }
            }
            ind += 1;
        }
        result
    }

    pub fn get_bridge_edges(&self) -> Vec<u32> {
        let mut result: Vec<u32> = Vec::new();
        self.enumerate_edges_with_bridge_info(&mut |from, to, is_bridge| {
            if is_bridge {
                result.push(from);
                result.push(to);
            }
        });
        result
    }

    pub fn get_cycle_parts(&self) -> Vec<u32> {
        let mut bridgeless_edges: Vec<u32> = Vec::new();
        self.enumerate_edges_with_bridge_info(&mut |from, to, is_bridge| {
            if !is_bridge {
                bridgeless_edges.push(from);
                bridgeless_edges.push(to);
            }
        });

        let bridgeless_graph = Self::from_edges(self.n, bridgeless_edges);
        let bridgeless_components = bridgeless_graph.get_components();
        let mut num_of_nodes_in_components: Vec<u32> = Vec::new();
        for i in 0..self.n {
            let component = bridgeless_components[i] as usize;
            while num_of_nodes_in_components.len() <= component {
                num_of_nodes_in_components.push(0);
            }
            num_of_nodes_in_components[component] += 1;
        }

        let mut result: Vec<u32> = Vec::new();
        for i in 0..self.n {
            let component = bridgeless_components[i] as usize;
            result.push(if num_of_nodes_in_components[component] > 1 {
                1
            } else {
                0
            })
        }
        result
    }

    fn enumerate_edges_with_bridge_info<F>(&self, f: &mut F) where
        F: FnMut(u32, u32, bool) -> () {
        let state = self.do_dfs();
        for from in 0..self.n {
            for i in 0..self.edges[from].len() {
                let to = self.edges[from][i];
                let is_bridge = state.t_low[to as usize] > state.t_in[from] || state.t_low[from] > state.t_in[to as usize]; 
                f(from as u32, to, is_bridge);
            }
        }
    }

    fn do_dfs(&self) -> DfsResult {
        let mut state = DfsState {
            visited: vec![false; self.n],
            t_in: vec![None; self.n],
            t_low: vec![None; self.n],
        };
        for v in 0..self.n {
            if !state.visited[v] {
                self.dfs(v as u32, None, 0, &mut state);
            }
        }
        state
    }

    fn dfs(&self, from: u32, parent: Option<u32>, t: u32, state: &mut DfsState) {
        state.visited[from as usize] = true;
        state.t_in[from as usize] = Some(t);
        state.t_low[from as usize] = Some(t);
        for i in 0..self.edges[from as usize].len() {
            let to = self.edges[from as usize][i];
            if Some(to) == parent {
                continue;
            }
            if state.visited[to as usize] {
                state.t_low[from as usize] = opt_min(state.t_low[from as usize], state.t_in[to as usize]);
            } else {
                self.dfs(to, Some(from), t + 1, state);
                state.t_low[from as usize] = opt_min(state.t_low[from as usize], state.t_low[to as usize]);
            }
        }
    }
}


struct DfsState {
    visited: Vec<bool>,
    t_in: Vec<Option<u32>>,
    t_low: Vec<Option<u32>>
}

type DfsResult = DfsState;


fn opt_min(a: Option<u32>, b: Option<u32>) -> Option<u32> {
    if let Some(av) = a {
        if let Some(bv) = b {
            if av < bv {
                Some(av)
            } else {
                Some(bv)
            }
        } else {
            Some(av)
        }
    } else {
        if let Some(bv) = b {
            Some(bv)
        } else {
            None
        }
    }
}


impl fmt::Display for Graph {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Graph({})\n", self.n)?;
        for (from, edges) in self.edges.iter().enumerate() {
            for to in edges.iter() {
                write!(f, "{} => {}\n", from, to)?;
            }
        }

        Ok(())
    }
}


#[cfg(test)]
mod tests {

    use super::*;

    #[test]
    fn creation() {
        let g1 = Graph::from_edges(5, vec![
            0, 1, 1, 0, 
            1, 2, 2, 1,
            2, 3, 3, 2,
            2, 4, 4, 2
        ]);
        let g2 = Graph::from_matrix(5, vec![
            0, 1, 0, 0, 0,
            1, 0, 1, 0, 0,
            0, 1, 0, 1, 1,
            0, 0, 1, 0, 0,
            0, 0, 1, 0, 0,
        ]);

        assert_eq!(g1.display(), g2.display());
    }

    #[test]
    fn components() {
        let g = Graph::from_matrix(6, vec![
            0, 1, 0, 0, 0, 0,
            1, 0, 1, 0, 0, 0,
            0, 1, 0, 0, 0, 0,
            0, 0, 0, 0, 1, 0,
            0, 0, 0, 1, 0, 0,
            0, 0, 0, 0, 0, 0,
        ]);

        assert_eq!(g.get_components(), vec![0, 0, 0, 1, 1, 2])
    }

    #[test]
    fn bridges() {
        let g = Graph::from_matrix(6, vec![
            0, 1, 1, 0, 0, 1,
            1, 0, 1, 0, 0, 0,
            1, 1, 0, 0, 0, 0,
            0, 0, 0, 0, 1, 1,
            0, 0, 0, 1, 0, 1,
            1, 0, 0, 1, 1, 0,
        ]);

        assert_eq!(g.get_bridge_edges(), vec![0, 5, 5, 0])
    }

    #[test]
    fn cycles() {
        let g = Graph::from_matrix(6, vec![
            0, 1, 1, 0, 0, 1,
            1, 0, 1, 0, 0, 0,
            1, 1, 0, 0, 0, 0,
            0, 0, 0, 0, 1, 0,
            0, 0, 0, 1, 0, 1,
            1, 0, 0, 0, 1, 0,
        ]);

        assert_eq!(g.get_cycle_parts(), vec![1, 1, 1, 0, 0, 0])
    }

    fn to_options(v: Vec<u32>) -> Vec<Option<u32>> {
        v.into_iter().map(|x| Some(x)).collect::<Vec<Option<u32>>>()
    }

    #[test]
    fn dfs() {
        let g = Graph::from_matrix(6, vec![
            0, 1, 1, 0, 0, 1,
            1, 0, 1, 0, 0, 0,
            1, 1, 0, 0, 0, 0,
            0, 0, 0, 0, 1, 1,
            0, 0, 0, 1, 0, 1,
            1, 0, 0, 1, 1, 0,
        ]);

        let result = g.do_dfs();
        assert_eq!(result.visited, vec![true; 6]);
        assert_eq!(result.t_in, to_options(vec![0, 1, 2, 2, 3, 1]));
        assert_eq!(result.t_low, to_options(vec![0, 0, 0, 1, 1, 1]));
    }

}